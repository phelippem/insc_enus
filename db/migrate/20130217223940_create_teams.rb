class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.belongs_to :event

      t.timestamps
    end
    add_index :teams, :event_id
  end
end

class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.boolean :active
      t.integer :maxperson
      t.integer :registration_ids

      t.timestamps
    end
  end
end

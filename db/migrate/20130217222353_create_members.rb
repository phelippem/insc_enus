class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :name
      t.string :email
      t.string :cpf
      t.string :rg
      t.string :phone
      t.belongs_to :team

      t.timestamps
    end
    add_index :members, :team_id
  end
end

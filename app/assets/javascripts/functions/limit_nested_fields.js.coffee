@app = @app || {}

@app.limitNestedFields = (limit) ->
  toggleAddLink = ->
    $addLink.toggle fieldsCount < maxFieldsCount

  fieldsCount = 0
  maxFieldsCount = parseInt(limit)
  $addLink = $("a.add_nested_fields")

  $(document).on "nested:fieldAdded", ->
    fieldsCount += 1
    toggleAddLink()

  $(document).on "nested:fieldRemoved", ->
    fieldsCount -= 1
    toggleAddLink()

  fieldsCount = $("form .fields").length
  toggleAddLink()

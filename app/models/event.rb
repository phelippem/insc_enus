class Event < ActiveRecord::Base

	has_many :teams

  	attr_accessible :active, :description, :maxperson, :name, :registration_ids

	validates :name, presence: true, length: {minimum: 3, maximum: 140}
	validates :description, presence: true
	validates :maxperson, presence: true
end
#encoding: utf-8
class Team < ActiveRecord::Base
  
  belongs_to :event
  has_many :members

  attr_accessible :name, :members_attributes

  accepts_nested_attributes_for :members, :allow_destroy => true


  # Validations
  validate :limit_of_members

  validates :name, :uniqueness => {:scope => @team, :case_sensitive => false }
  
  def limit_of_members
    errors.add(:members, 'Excedido o número máximo de pessoas por time') if members.length > event.maxperson
  end

end

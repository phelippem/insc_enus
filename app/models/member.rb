#encoding: utf-8
class Member < ActiveRecord::Base
	attr_accessible :cpf, :email, :name, :phone, :rg

	validates :name, presence: true, length: {minimum: 3, maximum: 140}

	validates :cpf, presence: true, 
				:uniqueness => { :scope => @event, :message => "Cada integrante só pode se inscrever em um único time por evento" }

	validates :email, presence: true,
				:uniqueness => { :scope => @event, :message => "Cada integrante só pode se inscrever em um único time por evento" } 
				
	validates :rg, presence: true

	validates :phone, presence: true

	validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :save
	
	before_save { |member| member.email = email.downcase }

	def formatted_email
		"#{@name} <#{@email}>"
	end
end

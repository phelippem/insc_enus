#encoding: utf-8

class EventsController < ApplicationController
  def index
    @events = Event.all

    respond_to do |format|
      format.html # chama -> index.html.erb
    end
  end

  def new
    @event = Event.new

    respond_to do |format|
      format.html # chama -> new.html.erb
    end
  end

  def create
    @event = Event.new(params[:event])

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: "Atração cadastrada com sucesso"}
      else
        format.html { render action: "new"}
      end
    end
  end

  def show
    @event = Event.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def edit
    @event = Event.find(params[:id])
  end

  def update
    @event = Event.find(params[:id])

    respond_to do |format|
      if @atracao.update_attributes(params[:atracao])
        format.html { redirect_to @event, notice: 'Atração atualizada com sucesso.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy

    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end
end

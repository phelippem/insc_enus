#encoding: utf-8

class TeamsController < ApplicationController

  def index
    @teams = Team.all
  end

  def show
    @team = Team.find(params[:id])
  end

  def new
    @event = Event.find(params[:event_id])
    @team = @event.teams.new
  end

  def edit
    @team = Team.find(params[:id])
    @event = @team.event
  end

  def create
    @event = Event.find(params[:event_id])
    @team = @event.teams.new(params[:team])
    
    if @team.save
      flash[:notice] = "ixi salvou"
      flash[:notice_class] = "class"
    else
      flash[:notice] = "ixi, deu merda"
      flash[:notice_class] = "class"
    end
    respond_with @team
  end

  def update
    @team = Team.find(params[:id])
    @team.update_attributes(params[:team])
    respond_with @team
  end

  def destroy
    @team = Team.find(params[:id])
    @team.destroy
    respond_with @team
  end

end
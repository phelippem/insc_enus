ActiveAdmin.register Event do
  form do |f|
      f.inputs "Details" do
        f.input :name
        f.input :description
        f.input :active, as: :boolean
        f.input :maxperson
      
      	f.buttons
      end
    end
end

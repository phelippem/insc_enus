# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :member do
    name "MyString"
    email "MyString"
    cpf "MyString"
    rg "MyString"
    phone "MyString"
  end
end
